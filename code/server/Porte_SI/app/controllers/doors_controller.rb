# coding: utf-8
class DoorsController < ApplicationController

  require 'httparty'

  def index
    @doors = Door.all
  end

  def show
    @door = Door.find(params[:id])
  end

  def new
    @door = Door.new
  end

  def create
    @door = Door.new
    @door.name = params[:door][:name]
    @door.ip= params[:door][:ip]
    @door.authorized_roles= params[:door][:authorized_roles]
    @door.is_open = false
    @door.save
    redirect_to "/doors/"
  end

  def edit
    @door = Door.find(params[:id])
  end

  def update
    @door = Door.find(params[:id])
    @door.name = params[:door][:name]
    @door.ip= params[:door][:ip]
    @door.authorized_roles= params[:door][:authorized_roles]
    @door.save
    redirect_to "/doors/"
  end

  def destroy
    @door = Door.find(params[:id])
    @door.destroy
    redirect_to "/doors/"
  end

  def toogle
    @door = Door.find(params[:id])
    if @door.is_open == true
      HTTParty.post("http://#{@door.ip}/" , body: {cmd: "close"})
      @door.is_open = false
      @door.save
    else
      HTTParty.post("http://#{@door.ip}/" , body: {cmd: "open"})
      @door.is_open = true
      @door.save
    end
    #Log utilisation porte
    redirect_to '/doors'
  end

end
