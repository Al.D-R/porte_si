class LogsController < ApplicationController

  def create
    @log = Log.new
    @log.content = params[:content]
    @log.date = Time.now
    @log.save
  end

  def destroy
    @log = Log.find(params[:id])
    @log.destroy
  end

  def index
    @logs = Log.all
  end

end
