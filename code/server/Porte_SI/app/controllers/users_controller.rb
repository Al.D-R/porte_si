class UsersController < ApplicationController

  def index
    @users = User.all
  end

  def new
    @user = User.new()
  end

  def create
    key = "ThERe@llyIncReDiblyippRessiveSupERkEynumBeR256"

    @user = User.new
    @user.name = params[:user][:name]
    @user.mail = params[:user][:mail]
    @user.password = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), key, params[:user][:password])
    @user.rfid = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), key, params[:user][:rfid])
    @user.code = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), key, params[:user][:code])
    @user.role = params[:user][:role]
    @user.save

    redirect_to "/users/"
  end

  def show
    @user = User.find(params[:id]).name
  end

  def edit
    @user =  User.find(params[:id])
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to "/users/"
  end

  def update
    key = "ThERe@llyIncReDiblyippRessiveSupERkEynumBeR256"

    @user = User.find(params[:id])
    @user.name = params[:user][:name]
    @user.mail = params[:user][:mail]
    @user.password = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), key, params[:user][:password])
    @user.rfid = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), key, params[:user][:rfid])
    @user.code = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), key, params[:user][:code])
    @user.role = params[:user][:role]
    @user.save

    redirect_to "/users/"
  end

end
