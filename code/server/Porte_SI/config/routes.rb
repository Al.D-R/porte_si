Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: 'pages#index'

  get 'login', to: 'pages#login'

  get 'logs', to: 'pages#logs'

  get 'permissions', to: 'pages#permissions'

  resources :users

  resources :doors

  get '/doors/:id/toogle', to: 'doors#toogle'

end
