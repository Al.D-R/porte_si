class CreateDoors < ActiveRecord::Migration[6.0]
  def change
    create_table :doors do |t|
      t.string :name
      t.string :ip
      t.string :authorized_roles
    end
  end
end
