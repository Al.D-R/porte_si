class AddIsOpenToDoors < ActiveRecord::Migration[6.0]
  def change
    add_column :doors, :is_open, :bool
  end
end
