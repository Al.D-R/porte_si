class AddLastUserToDoors < ActiveRecord::Migration[6.0]
  def change
    add_column :doors, :last_user, :string
  end
end
