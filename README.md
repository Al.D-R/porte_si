# Projet de SI porte blindée automatisée

## Description:
Programme basé sur le modèle client/serveur pour commander un réseau de portes automatisées par un serveur central grâce à une interface d'administration web.

## Dépendances : 
### Serveur :
- Ruby >= 2.5.7
- PostgreSQL >= 12.1
- Curl >= 7.66.0
### Client: 
- Arduino.h
- Keypad.h
- Ethernet.h
